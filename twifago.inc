<?php 

/**
 * @file
 *   Administrative messages.
 */

/**
 * Check jquery versión value in jquery_update module.
 */
function twifago_check_jqv() {
  if (user_access('administer site configuration') && floatval(variable_get('jquery_update_jquery_version')) < 1.7) {
    drupal_set_message(t('Update your jquery version to make <i>twifago</i> module function, must be 1.7 or higher, actual version is '.variable_get('jquery_update_jquery_version').', setup this in !jqup_conf', 
        array('!jqup_conf' => l(t('jquery_module configuration page'),'admin/config/development/jquery_update')
          )
        ), 
      'error'
    );
  } 
}

/**
 * First instructions.
 */
function twifago_first_instructions() {
	drupal_set_message(t('<i>twifago</i> module create a block that implenet the Share Button, only enable this at <strong>!block_page_configure</strong>', 
	    array('!block_page_configure' => l(t('Blocks configuration page'),'admin/structure/block')
	      )
	    ), 
	  'status'
	);
}